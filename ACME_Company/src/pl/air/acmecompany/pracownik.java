package pl.air.acmecompany;


public class pracownik {
    private int id;

    private String imie;

    private String drugieImie;

    private String nazwisko;

    private int pensja;

    private jednostka jednostka;

    private stanowisko stanowisko;

    private placa placa;

    public pracownik() {
    }

    public int getId() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.id;
    }

    public void setId(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.id = value;
    }

    public String getImie() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.imie;
    }

    public void setImie(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.imie = value;
    }

    public String getDrugieImie() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.drugieImie;
    }

    public void setDrugieImie(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.drugieImie = value;
    }

    public String getNazwisko() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nazwisko;
    }

    public void setNazwisko(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nazwisko = value;
    }

    public int getPensja() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.pensja;
    }

    public void setPensja(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.pensja = value;
    }

    public jednostka getJednostka() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.jednostka;
    }

    public void setJednostka(jednostka value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.jednostka = value;
    }

    public stanowisko getStanowisko() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.stanowisko;
    }

    public void setStanowisko(stanowisko value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.stanowisko = value;
    }

    public placa getPlaca() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.placa;
    }

    public void setPlaca(placa value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.placa = value;
    }

}

package pl.air.acmecompany;

import java.util.ArrayList;
import java.util.List;

public class kierownik {
    private String drugieImie;

    private String nazwisko;

    private int pensja;

    private int id;

    private String imie;

    private jednostka jednostka;

    private List<pracownik> pracownicy = new ArrayList<pracownik> ();

    public kierownik() {
    }

    public String getDrugieImie() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.drugieImie;
    }

    public void setDrugieImie(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.drugieImie = value;
    }

    public void setNazwisko(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nazwisko = value;
    }

    public String getNazwisko() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nazwisko;
    }

    public int getPensja() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.pensja;
    }

    public void setPensja(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.pensja = value;
    }

    public int getId() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.id;
    }

    public void setId(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.id = value;
    }

    public String getImie() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.imie;
    }

    public void setImie(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.imie = value;
    }

    public jednostka getJednostka() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.jednostka;
    }

    public void setJednostka(jednostka value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.jednostka = value;
    }

    public List<pracownik> getPracownicy() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.pracownicy;
    }

    public void setPracownicy(List<pracownik> value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.pracownicy = value;
    }

}

package pl.air.acmecompany;


public class jednostka {
    private String nazwa;

    private int id;

    private int nrTelefonu;

    private String ulica;

    private int nrBudynku;

    private int nrLokalizacji;

    private String kod;

    private String miejscowosc;

    private String typ;

    private int nrHierarchi;

    private jednostka jednostka;

    public jednostka() {
    }

    public String getNazwa() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nazwa;
    }

    public void setNazwa(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nazwa = value;
    }

    public int getId() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.id;
    }

    public void setId(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.id = value;
    }

    public int getNrTelefonu() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nrTelefonu;
    }

    public void setNrTelefonu(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nrTelefonu = value;
    }

    public String getUlica() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.ulica;
    }

    public void setUlica(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.ulica = value;
    }

    public int getNrBudynku() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nrBudynku;
    }

    public void setNrBudynku(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nrBudynku = value;
    }

    public int getNrLokalizacji() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nrLokalizacji;
    }

    public void setNrLokalizacji(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nrLokalizacji = value;
    }

    public void setMiejscowosc(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.miejscowosc = value;
    }

    public String getMiejscowosc() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.miejscowosc;
    }

    public String getKod() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.kod;
    }

    public void setKod(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.kod = value;
    }

    public String getTyp() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.typ;
    }

    public void setTyp(String value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.typ = value;
    }

    public int getNrHierarchi() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.nrHierarchi;
    }

    public void setNrHierarchi(int value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.nrHierarchi = value;
    }

    public jednostka getJednostka() {
        // Automatically generated method. Please delete this comment before entering specific code.
        return this.jednostka;
    }

    public void setJednostka(jednostka value) {
        // Automatically generated method. Please delete this comment before entering specific code.
        this.jednostka = value;
    }

}
